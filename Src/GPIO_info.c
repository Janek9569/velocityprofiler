#include "GPIO_info.h"

void gpio_set(GPIO_info* gpio)
{
	HAL_GPIO_WritePin(gpio->port, gpio->pin, GPIO_PIN_SET);
}

void gpio_reset(GPIO_info* gpio)
{
	HAL_GPIO_WritePin(gpio->port, gpio->pin, GPIO_PIN_RESET);
}

void gpio_set_state(GPIO_info* gpio, uint8_t state)
{
	GPIO_PinState pin_state = state == 0 ? GPIO_PIN_SET : GPIO_PIN_RESET;
	HAL_GPIO_WritePin(gpio->port, gpio->pin, pin_state);
}
