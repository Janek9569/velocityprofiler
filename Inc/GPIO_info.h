#ifndef GPIO_INFO_H_
#define GPIO_INFO_H_

#include "stm32f1xx_hal.h"

typedef struct
{
	GPIO_TypeDef* port;
	uint16_t pin;
}GPIO_info;

void gpio_set(GPIO_info* gpio);
void gpio_reset(GPIO_info* gpio);
void gpio_set_state(GPIO_info* gpio, uint8_t state);
#endif
