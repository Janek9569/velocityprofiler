#ifndef _VELOCITYPROFILER_H
#define _VELOCITYPROFILER_H
#include <stdint.h>
#include <string.h>
#include "stm32f1xx_hal.h"
#include "../Settings.h"
#include "../Timer/Timer.h"
#include "../PID/PID.h"
#include "../TimeFunction/TimeFunction.h"
#include "GPIO_info.h"

#define BASE_FREQ 33333.3		//HIGH timer freq
#define STEPPER_FREQ 3750.0
#define BASE_PWM_FREQ 18750		//PWM freq
#define MAX_DC_PROILERS 5
#define PWM_MAX 1600

#define FORWARD 1
#define BACK (-1)
typedef int8_t Direction;

typedef enum
{
	RUNNING,
	STOPPING,
	SUSPENDED,
	STOPPED
}ProfilerState;

typedef enum
{
	STEPPER,
	DCMOTOR
} EngineType;

typedef enum
{
	STEPPER_RESOLUTION_FULL,
	STEPPER_RESOLUTION_HALF,
	STEPPER_RESOLUTION_QUARTER,
	STEPPER_RESOLUTION_EIGHT_STEP,
	STEPPER_RESOLUTION_SIXTEETH_STEP,
	STEPPER_RESOLUTION_LAST
} StepperResolution;

typedef struct
{
	/* stepper */
	GPIO_info EN;		//logic high -> disabled
	GPIO_info DIR;		//logic low  -> forward
	GPIO_info STEP; 	//send pulse to move motor
	GPIO_info SLEEP;	//logic low -> sleep mode
	GPIO_info RST;		//logic high -> reset
	GPIO_info MS1;		//resolution selection
	GPIO_info MS2;		//resolution selection
	GPIO_info MS3;		//resolution selection

	/* DC motor */
	GPIO_info EN_A;		//enable A channel
	GPIO_info IN_A;		//A channel input
	GPIO_info EN_B;		//ebanle B channel
	GPIO_info IN_B;		//B channel input
	GPIO_info MOT_CS;	//Motor chip selection
} ProfilerIO;

struct _velocityProfiler;
typedef struct _velocityProfiler VelocityProfiler;

struct _velocityProfiler
{
	ProfilerState state;
	int32_t destStep;			//destination step number
	int32_t actualStep;		//actual step
	float actSetPoint;			//steps/s
	float velocityBeforeStop;		//steps/s

	volatile uint32_t* CCR;
	uint32_t PWMChannel;
	StepperResolution resolution;

	ProfilerIO IO;
	float maxVelocity;					//steps/sec
	float minVelocity;					//steps/sec
	float minPIDResp;
	volatile float actualVelocity;
	float acc;						//steps/s+^2
	TIM_HandleTypeDef* htim;			//freq timer (for stepper motors)
	Timer* tim;							//timer for debugging or for measuring velocity
	PID* pid;
	TimeFunction* wakeSuspendedF;		//for DC motors to wakeup suspended motor
	TimeFunction* DCwatchdog;			//for DC motors to wakeup RUNNING motor
	uint8_t (*moveSteps) (VelocityProfiler*, int32_t);			//run engine
	void (*update) (VelocityProfiler* prof);
};

void initVelocityProfilerStepper(VelocityProfiler* prof, uint32_t _maxVelocity, uint32_t minVelocity, uint32_t _acc, TIM_HandleTypeDef* htim, ProfilerIO* IO);
void initVelocityProfilerDCMotor(VelocityProfiler* prof, volatile uint32_t* CCR, uint32_t _maxVelocity, uint32_t minVelocity, uint32_t _acc, ProfilerIO* IO);


extern VelocityProfiler testProfiler;
/*************** DEBUGGING **************/
#ifdef DEBUG_VELOCITY_PROFILER
#include "../SD/SD.h"
#include "tim.h"
extern VelocityProfiler testProfiler;
extern volatile int32_t actsp[1000];
extern volatile uint64_t t[1000];
extern volatile int32_t step[1000];
extern volatile int32_t veloc[1000];
extern volatile int16_t ccr[1000];
extern volatile uint16_t dbg_it;
#endif
void startDebugging(EngineType type);
void saveToSD();
void updateDebugInfo();
/*************** DEBUGGING **************/

void testParams(VelocityProfiler* prof);
#endif
