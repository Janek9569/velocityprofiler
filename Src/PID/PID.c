#include "PID.h"
#include <stdlib.h>

static double computePID(PID* pid, uint32_t SP, uint32_t PV)
{
	uint32_t error;
	uint32_t pResp;
	uint32_t dResp;
	uint32_t iResp = 0;
	uint32_t ret;

	error = SP - PV;

	if (error > pid->maxError)
		pResp = INT16_MAX;
	else if (error < -pid->maxError)
		pResp = -INT16_MAX;
	else
		pResp = pid->P * error;

	if(pid->I != 0.0)
	{
		pid->sumError += error;
		if(pid->sumError > pid->maxSumError)
		{
			iResp = (INT32_MAX / 2);
			pid->sumError = pid->maxSumError;
		}
		else if(pid->sumError < -pid->maxSumError)
		{
			iResp = -(INT32_MAX / 2);
			pid->sumError = -pid->maxSumError;
		}
		else
			iResp = pid->I* pid->sumError;
	}

	dResp = pid->D * (pid->lastPV - PV);

	pid->lastPV = PV;

	ret = (pResp + iResp + dResp);

	if(ret > INT16_MAX)
		ret = INT16_MAX;
	else if(ret < -INT16_MAX)
		ret = -INT16_MAX;

	return ret;
}

void initPID(PID* pid, uint32_t P, uint32_t I, uint32_t D)
{
	pid->compute = computePID;
	pid->sumError = 0.0;
	pid->lastPV = 0.0;
	pid->P = P;
	pid->I = I;
	pid->D = D;
	pid->maxError = INT16_MAX / (P + 1);
	pid->maxSumError = (INT32_MAX / 2) / (I + 1);
}

PID* initNewPID(uint32_t P, uint32_t I, uint32_t D)
{
	PID* newPID = malloc(sizeof(PID));
	initPID(newPID,P,I,D);
	return newPID;
}
