#include "VelocityProfiler.h"
#include <stdlib.h>
#include <math.h>

/**************** PRIVATE ***************/
static uint8_t _dcProfilerIter = 0;
static VelocityProfiler* registeredDCProfilers[MAX_DC_PROILERS];
static void intChangeFreq(VelocityProfiler* prof);
uint8_t makeStepperStep(VelocityProfiler* prof);
static uint8_t stepperShouldStopping(VelocityProfiler* prof);

/**** DC MOTOR ****/

static uint8_t DCshouldStopping(VelocityProfiler* prof);
static void getActualDCMotorVelocity(VelocityProfiler* prof);
static void makeDCMotorStep(VelocityProfiler* prof);
static void DCwakeup(void* prf);
static void updateDCProfilers();
/**************** PRIVATE ***************/

extern VelocityProfiler StepperProfiler;

/*************** CONFIGURE **************/
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	if(htim->Instance == TIM3)
	{
		updateHighFreqTimers();
		updateTimeFunctions();
	}
	else if(htim->Instance == TIM1)
	{
		StepperProfiler.update(&StepperProfiler);
	}
}

void HAL_SYSTICK_Callback()
{
	updateLowFreqTimers();
}

static void stepperStep(VelocityProfiler* vp, Direction dir)
{
	gpio_set(&vp->IO.STEP);
	gpio_reset(&vp->IO.STEP);
}

static uint8_t stepperLostStep(VelocityProfiler* prof)
{
	return 0;
}

/**** DC MOTOR ****/

static void changePWM(VelocityProfiler* vp, uint32_t PV)
{
	*(vp->CCR) = PV;
}

static void setForward(VelocityProfiler* vp)
{
	gpio_set(&vp->IO.IN_A);
	gpio_reset(&vp->IO.IN_B);
}

static void setBack(VelocityProfiler* vp)
{
	gpio_reset(&vp->IO.IN_A);
	gpio_set(&vp->IO.IN_B);
}
/*************** CONFIGURE **************/



/*************** DEBUGGING **************/
#ifdef DEBUG_VELOCITY_PROFILER
VelocityProfiler testProfiler;
volatile int32_t actsp[1000];
volatile uint64_t t[1000];
volatile int32_t step[1000];
volatile int32_t veloc[1000];
volatile int16_t ccr[1000];
volatile uint16_t dbg_it = 0;

void startDebugging(EngineType type)
{
	if(type == DCMOTOR)
	{
		initVelocityProfilerDCMotor(&testProfiler,230,8,20,&htim2,MOTOR1);
		testProfiler.minPIDResp = 105;
	}
	else
	{
		initVelocityProfilerStepper(&testProfiler,200,5,5,&htim6,MOTOR2);
	}
	testProfiler.moveSteps(&testProfiler, 500);
}

void saveToSD()
{
	int i;
	FIL fil;
	f_open(&fil, "expet.txt",  FA_OPEN_ALWAYS | FA_OPEN_APPEND | FA_WRITE);
	UINT bw;
	char number[20]; memset(number,0x00,20);
	for(i=0; i < 800; ++i)
	{
		itoa(actsp[i],number,10);
		f_write(&fil, number, strlen(number), &bw);
		f_write(&fil, ", ", strlen(", "), &bw);
	}
	f_write(&fil, "\n\r", strlen("\r\n"), &bw);
	for(i=0; i < 800; ++i)
	{
		itoa(step[i],number,10);
		f_write(&fil, number, strlen(number), &bw);
		f_write(&fil, ", ", strlen(", "), &bw);
	}
	f_write(&fil, "\n\r", strlen("\r\n"), &bw);
	for(i=0; i < 800; ++i)
	{
		itoa(veloc[i],number,10);
		f_write(&fil, number, strlen(number), &bw);
		f_write(&fil, ", ", strlen(", "), &bw);
	}
	f_write(&fil, "\n\r", strlen("\r\n"), &bw);
	for(i=0; i < 800; ++i)
	{
		itoa(ccr[i],number,10);
		f_write(&fil, number, strlen(number), &bw);
		f_write(&fil, ", ", strlen(", "), &bw);
	}
	f_write(&fil, "\n\r", strlen("\r\n"), &bw);
	for(i=0; i < 800; ++i)
	{
		itoa(t[i],number,10);
		f_write(&fil, number, strlen(number), &bw);
		f_write(&fil, ", ", strlen(", "), &bw);
	}
	f_close(&fil);
}

void updateDebugInfo()
{
	VelocityProfiler* prof = &testProfiler;
	if(dbg_it > 980) return;
	t[dbg_it] = t[dbg_it-1]+ prof->tim->getElapsedTime(prof->tim);
	actsp[dbg_it] = prof->actSetPoint;
	veloc[dbg_it] = prof->actualVelocity;
	ccr[dbg_it] = prof->htim->Instance->CCR1;
	step[dbg_it++] = prof->actualStep;
}
#else

void saveToSD(){}
void updateDebugInfo(){}

void startDebugging(EngineType type)
{
	(void)type;
}
#endif
/*************** DEBUGGING **************/



/***************  PRIVATE STEPPER  **************/
static void intChangeFreq(VelocityProfiler* prof)
{
	if(prof->state == STOPPED)
		HAL_TIM_Base_Stop_IT(prof->htim);
	uint32_t prescaler = STEPPER_FREQ / fabs(prof->actSetPoint);
	prof->htim->Instance->ARR = prescaler;
}

uint8_t makeStepperStep(VelocityProfiler* prof)
{
	if(prof->actSetPoint < 0)
	{
		--(prof->actualStep);
		stepperStep(prof, BACK);
		if(stepperLostStep(prof))
		{
			++(prof->actualStep);
			return 0;
		}
	}
	else
	{
		++(prof->actualStep);
		stepperStep(prof, FORWARD);
		if(stepperLostStep(prof))
		{
			--(prof->actualStep);
			return 0;
		}
	}
	return 1;
}

static uint8_t stepperShouldStopping(VelocityProfiler* prof)
{
	Direction dir = prof->destStep > prof->actualStep ? FORWARD: BACK;

	if((prof->actSetPoint > 0 && dir == BACK) || (prof->actSetPoint < 0 && dir == FORWARD))
		return 0;

	int32_t leftSteps = prof->destStep - prof->actualStep;
	float vNext = leftSteps < 0 ? -prof->minVelocity : prof->minVelocity;

	leftSteps = abs(leftSteps)-1;
	int32_t S = ((prof->actSetPoint - vNext)*(prof->actSetPoint + vNext))/(2*prof->acc);
	if(S >= leftSteps)
		return 1;
	return 0;
}

static void stepper_enable(VelocityProfiler* prof)
{
	gpio_reset(&prof->IO.EN);
}

static void stepper_disable(VelocityProfiler* prof)
{
	gpio_set(&prof->IO.EN);
}

static void stepper_set_resolution(VelocityProfiler* prof, StepperResolution resolution)
{
	static uint8_t resolution_states[STEPPER_RESOLUTION_LAST][3] =
	{
		{0, 0, 0}, //STEPPER_RESOLUTION_FULL
		{1, 0, 0}, //STEPPER_RESOLUTION_HALF
		{0, 1, 0}, //STEPPER_RESOLUTION_QUARTER
		{1, 1, 0}, //STEPPER_RESOLUTION_EIGHT_STEP
		{1, 1, 1}  //STEPPER_RESOLUTION_SIXTEETH_STEP
	};
	uint8_t* MS = resolution_states[resolution];
	prof->resolution = resolution;

	gpio_set_state(&prof->IO.MS1, MS[0]);
	gpio_set_state(&prof->IO.MS1, MS[1]);
	gpio_set_state(&prof->IO.MS1, MS[2]);
}

static void stepper_sleep(VelocityProfiler* prof, uint8_t sleep_wake)
{
	if(sleep_wake == 1)
		gpio_reset(&prof->IO.SLEEP);
	else
		gpio_set(&prof->IO.SLEEP);
}

static void stepper_reset(VelocityProfiler* prof)
{
	gpio_set(&prof->IO.RST);
	gpio_reset(&prof->IO.RST);
	gpio_set(&prof->IO.RST);
}
/***************  PRIVATE STEPPER  **************/



/***************  PUBLIC STEPPER  **************/

static uint8_t stepperMoveSteps(VelocityProfiler* prof, int32_t destStep)
{
	if(prof->tim)
		prof->tim->resetTimer(prof->tim);
	prof->destStep = destStep;

	if(prof->state == STOPPED)
	{
		prof->actualVelocity = 0;

		if(prof->tim)
			prof->tim->resetTimer(prof->tim);

		if(prof->actualStep < destStep )
			prof->actSetPoint = prof->minVelocity;
		else
			prof->actSetPoint = -(prof->minVelocity);
	}

	makeStepperStep(prof);
	if(prof->state == STOPPED)
	{
		stepper_sleep(prof, 0);
		HAL_TIM_Base_Start_IT(prof->htim);
		prof->state = RUNNING;
		intChangeFreq(prof);
	}
	else
		prof->state = RUNNING;

	updateDebugInfo();
	return 1;
}

static void stepperUpdate(VelocityProfiler* prof)
{
	if(prof->state != STOPPED)
	{
		Direction dir = prof->destStep > prof->actualStep ? FORWARD: BACK;
		if((prof->destStep == prof->actualStep) && (prof->actSetPoint < 0))
			dir = BACK;
		uint8_t shouldStopping = stepperShouldStopping(prof);
		float velocitQuantum = fabs((prof->acc)/prof->actSetPoint);

		if(shouldStopping && dir == FORWARD)
			velocitQuantum *= -1;
		else if(!shouldStopping && dir == BACK)
			velocitQuantum *= -1;

		prof->actSetPoint += velocitQuantum;

		//freq overflow ?
		if(prof->actSetPoint >= prof->maxVelocity)
			prof->actSetPoint = prof->maxVelocity;
		if(prof->actSetPoint <= -prof->maxVelocity)
			prof->actSetPoint = -prof->maxVelocity;

		if((prof->destStep == prof->actualStep) && (fabs(prof->actSetPoint) <= fabs(prof->minVelocity*1.5)))
		{
			stepper_sleep(prof, 1);
			prof->state = STOPPED;
			prof->actSetPoint = 0;
			intChangeFreq(prof);
			return;
		}

		if(!makeStepperStep(prof))
			prof->actSetPoint -= velocitQuantum;

		intChangeFreq(prof);
		updateDebugInfo();
	}
}

void initVelocityProfilerStepper(VelocityProfiler* prof, uint32_t _maxVelocity, uint32_t minVelocity ,uint32_t _acc, TIM_HandleTypeDef* htim, ProfilerIO* IO)
{
	memset(prof, 0x00 ,sizeof(VelocityProfiler));
	prof->state = STOPPED;
	prof->maxVelocity = _maxVelocity;
	prof->acc = _acc;
	prof->htim = htim;
	prof->minVelocity = minVelocity;
	prof->moveSteps = stepperMoveSteps;
	prof->update = stepperUpdate;
	prof->IO = *IO;

	//rest timer
	intChangeFreq(prof);

	//set default step state
	gpio_reset(&IO->STEP);

	stepper_disable(prof);
	stepper_reset(prof);
	stepper_set_resolution(prof, STEPPER_RESOLUTION_QUARTER);
	stepper_sleep(prof, 1);
	stepper_enable(prof);

#ifdef DEBUG_VELOCITY_PROFILER
	prof->tim = registerTimer(HIGHFREQ);
#endif
}
/***************  PUBLIC STEPPER  **************/



/***************  PRIVATE DC MOTOR **************/
static uint8_t DCshouldStopping(VelocityProfiler* prof)
{
	Direction dir = prof->destStep > prof->actualStep ? FORWARD: BACK;
	if((prof->actSetPoint > 0 && dir == BACK) || (prof->actSetPoint < 0 && dir == FORWARD))
		return 0;

	int32_t leftSteps = prof->destStep - prof->actualStep;
	float vNext = leftSteps < 0 ? -prof->minVelocity : prof->minVelocity;

	leftSteps = abs(leftSteps)-1;
	float S = ((prof->actualVelocity - vNext)*(prof->actualVelocity + vNext))/(2*prof->acc);
	if(S >= leftSteps)
		return 1;
	return 0;
}

static void getActualDCMotorVelocity(VelocityProfiler* prof)
{
	if(prof->tim->overlfow)
		prof->actualVelocity = 0.0;

	updateDebugInfo();
	float elapsedTime = prof->tim->getElapsedTime(prof->tim);
	if(fabs(prof->actualVelocity) > prof->maxVelocity*5)
	{
		prof->actualVelocity = ((BASE_FREQ)/elapsedTime);
	}
	else
		prof->actualVelocity = 0.7*((BASE_FREQ)/elapsedTime) + 0.3*(fabs(prof->actualVelocity));
	if(prof->actSetPoint < 0)
		prof->actualVelocity *= -1;
	prof->tim->resetTimer(prof->tim);
}

static void makeDCMotorStep(VelocityProfiler* prof)
{

	if(prof->actSetPoint > 0)
		setForward(prof);
	else
		setBack(prof);

	int32_t PIDResponse = prof->pid->compute(prof->pid,fabs(prof->actSetPoint), fabs(prof->actualVelocity));

	if(PIDResponse < prof->minPIDResp)
		PIDResponse = prof->minPIDResp;

	if(PIDResponse > PWM_MAX)
		PIDResponse= PWM_MAX;

	changePWM(prof, PIDResponse);
}

static void DCwakeup(void* prf)
{
	VelocityProfiler* prof = prf;
	Direction dir = prof->destStep > prof->actualStep ? FORWARD: BACK;
	(dir == FORWARD) ? (prof->actSetPoint = prof->minVelocity) : (prof->actSetPoint = -prof->minVelocity);
	makeDCMotorStep(prof);
	prof->state = RUNNING;
	prof->actualVelocity = 0;

	if(prof->actualStep < prof->destStep )
		prof->actSetPoint = prof->minVelocity;
	else
		prof->actSetPoint = -prof->minVelocity;

	if(prof->actSetPoint > 0)
		setForward(prof);
	else
		setBack(prof);

	changePWM(prof, prof->minPIDResp);
	updateDebugInfo();

	prof->tim->resetTimer(prof->tim);
	registerStaticTimeFunction(prof->DCwatchdog);
}

static void DCwakeRunning(void* prf)
{
	VelocityProfiler* prof = prf;
	if(prof->state == RUNNING)
	{
		prof->actSetPoint = 0;
		prof->pid->lastPV = 0;

		uint32_t PWM = *(prof->CCR) += 20;
		if(PWM > PWM_MAX)
			PWM= PWM_MAX;

		*(prof->CCR) = PWM;
	}
	prof->DCwatchdog->timer->resetTimer(prof->DCwatchdog->timer);
	prof->tim->resetTimer(prof->tim);
}

static void updateDCProfilers()
{
	uint8_t i;
	for(i = 0; i < _dcProfilerIter; ++i)
	{
		VelocityProfiler* prof = registeredDCProfilers[i];
		getActualDCMotorVelocity(prof);
		if(prof->state == STOPPED || prof->state == SUSPENDED)
		{
			if(prof->velocityBeforeStop > 0)
				prof->actualStep++;
			else
				prof->actualStep--;
		}
		else
		{
			if(prof->actSetPoint > 0)
				prof->actualStep++;
			else
				prof->actualStep--;
		}
		if(prof->state != SUSPENDED)
		{
			prof->update(registeredDCProfilers[i]);
		}
	}
}
/***************  PRIVATE DC MOTOR **************/



/***************  PUBLIC DC MOTOR **************/
static uint8_t DCMoveSteps(VelocityProfiler* prof, int32_t destStep)
{
	prof->DCwatchdog->autoRestart = 1;
	prof->DCwatchdog->timer->resetTimer(prof->DCwatchdog->timer);
	registerStaticTimeFunction(prof->DCwatchdog);

	prof->destStep = destStep;
	if(prof->state == STOPPED)
	{
		gpio_set(&prof->IO.EN_A);
		gpio_set(&prof->IO.EN_B);

		prof->actualVelocity = 0;
		prof->tim->resetTimer(prof->tim);
		if(prof->actualStep < destStep )
			prof->actSetPoint = prof->minVelocity;
		else
			prof->actSetPoint = -prof->minVelocity;
	}
	else getActualDCMotorVelocity(prof);

	if(prof->actSetPoint > 0)
		setForward(prof);
	else
		setBack(prof);

	changePWM(prof ,prof->minPIDResp);
	prof->state = RUNNING;
	return 1;
}

static void DCupdate(VelocityProfiler* prof)
{
	if(prof->state != STOPPED)
	{
		prof->DCwatchdog->timer->resetTimer(prof->DCwatchdog->timer);
		//if(prof->actualStep == 300)
		//	prof->destStep = 100;
		Direction dir = prof->destStep > prof->actualStep ? FORWARD: BACK;
		if((prof->destStep == prof->actualStep) && (prof->actSetPoint < 0))
			dir = BACK;

		uint8_t shouldStopping = DCshouldStopping(prof);
		int32_t velocitQuantum = fabs((prof->acc)/prof->actualVelocity);

		if(shouldStopping && dir == FORWARD)
			velocitQuantum *= -1;
		else if(!shouldStopping && dir == BACK)
			velocitQuantum *= -1;

		prof->actSetPoint += velocitQuantum;

		//freq overflow ?
		if(prof->actSetPoint >= prof->maxVelocity)
			prof->actSetPoint = prof->maxVelocity;
		if(prof->actSetPoint <= -prof->maxVelocity)
			prof->actSetPoint = -prof->maxVelocity;

		if((prof->destStep == prof->actualStep) && (fabs(prof->actSetPoint) <= fabs(prof->minVelocity*1.5)))
		{
			prof->velocityBeforeStop = prof->actualVelocity;
			prof->state = STOPPED;
			prof->actSetPoint = 0;
			changePWM(prof, 0);
			unregisterTimeFunction(prof->DCwatchdog);
			return;
		}

		uint8_t SPUnderMin = fabs(prof->actSetPoint) < prof->minVelocity;
		if( (prof->actSetPoint >= 0 && SPUnderMin && dir == BACK) || (prof->actSetPoint <= 0 && SPUnderMin && dir == FORWARD))
		{
			unregisterTimeFunction(prof->DCwatchdog);
			prof->velocityBeforeStop = prof->actualVelocity;
			prof->state = SUSPENDED;
			prof->actSetPoint = 0;
			changePWM(prof, 0);
			prof->pid->lastPV = 0;
			registerStaticTimeFunction(prof->wakeSuspendedF);
			return;
		}

		makeDCMotorStep(prof);
	}
}

void initVelocityProfilerDCMotor(VelocityProfiler* prof, volatile uint32_t* CCR, uint32_t _maxVelocity, uint32_t minVelocity, uint32_t _acc, ProfilerIO* IO)
{
	memset(prof, 0x00 ,sizeof(VelocityProfiler));
	prof->state = STOPPED;
	prof->maxVelocity = _maxVelocity;
	prof->acc = _acc;
	prof->htim = NULL;
	prof->minVelocity = minVelocity;
	prof->moveSteps = DCMoveSteps;
	prof->update = DCupdate;
	prof->IO = *IO;
	prof->CCR = CCR;
	prof->PWMChannel = PWMChannel;

	prof->tim = registerTimer(HIGHFREQ);
	prof->pid = initNewPID(50,0,50);
	prof->pid->lastPV = prof->minPIDResp;
	(*CCR) = 0;

	TimeFunction* tF = malloc(sizeof(TimeFunction));
	prof->wakeSuspendedF = tF;
	tF->arg = prof;
	tF->autoRestart = 0;
	tF->f = DCwakeup;
	tF->isStatic = 1;
	tF->ticks = 500;
	tF->timer = registerTimer(LOWFREQ);

	TimeFunction* tF2 = malloc(sizeof(TimeFunction));
	prof->DCwatchdog = tF2;
	tF2->arg = prof;
	tF2->autoRestart = 0;
	tF2->f = DCwakeRunning;
	tF2->isStatic = 1;
	tF2->ticks = 100;
	tF2->timer = registerTimer(LOWFREQ);

	registeredDCProfilers[_dcProfilerIter++] = prof;

	setForward(prof);
	gpio_reset(&prof->IO.EN_A);
	gpio_reset(&prof->IO.EN_B);

}
/***************  PUBLIC DC MOTOR **************/

#ifdef TESTPARAMS
VelocityProfiler* testProf;
void testParams(VelocityProfiler* prof)
{
	testProf = prof;
	gpio_set(&prof->IO.EN_A);
	gpio_set(&prof->IO.EN_B);

	changePWM(prof, 140);
}
#endif

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
#ifdef TESTPARAMS
	static uint32_t testVelocity[1500] = {0};
	static uint32_t i = 0;
#endif
	if(GPIO_Pin == ENCODER_Pin)
	{
#ifdef TESTPARAMS
		getActualDCMotorVelocity(testProf);
		testVelocity[++i] = testProf->actualVelocity;
#else
		updateDCProfilers();
#endif
	}
}











