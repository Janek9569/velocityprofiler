
#ifndef TIMER_TIMER_H_
#define TIMER_TIMER_H_

#include <stdint.h>

#define MAX_HFREQ_TIMERS 15
#define MAX_LFREQ_TIMERS 15

typedef enum
{
	LOWFREQ,
	HIGHFREQ
}TimerType;

struct _timer;
typedef struct _timer Timer;
typedef struct _timer
{
	volatile uint64_t tick;
	uint8_t overlfow;

	uint64_t (*getElapsedTime) (Timer* tim);
	uint64_t (*resetTimer) (Timer* tim);
}Timer;

Timer* registerTimer(TimerType type);
void updateLowFreqTimers();
void updateHighFreqTimers();
#endif
