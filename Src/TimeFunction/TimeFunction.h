

#ifndef TIMEFUNCTION_TIMEFUNCTION_H_
#define TIMEFUNCTION_TIMEFUNCTION_H_


#include <stdint.h>
#include "../Timer/Timer.h"
#define MAX_TIME_FUNCTIONS 5

struct _timeFunction;
typedef struct _timeFunction TimeFunction;
typedef struct _timeFunction
{
	void (*f) (void*);
	void* arg;
	uint64_t ticks;
	uint8_t autoRestart;
	Timer* timer;
	uint8_t isStatic;
}TimeFunction;


void registerTimeFunction(void (*f) (void*), void* arg, uint8_t autoRestart, uint64_t ticks, TimerType timerType);
void registerStaticTimeFunction(TimeFunction* tF);
void unregisterTimeFunction(TimeFunction* tF);
void updateTimeFunctions();


#endif
