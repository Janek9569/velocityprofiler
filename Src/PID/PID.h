
#ifndef PID_PID_H_
#define PID_PID_H_

#include <stdint.h>

struct _pid;
typedef struct _pid PID;
typedef struct _pid
{
	uint32_t lastPV;
	uint32_t sumError;
	uint32_t P,I,D;
	uint32_t maxError;
	uint32_t maxSumError;

	uint32_t (*compute)(PID*, uint32_t, uint32_t);
}PID;

void initPID(PID* pid, uint32_t P, uint32_t I, uint32_t D);
PID* initNewPID(uint32_t P, uint32_t I, uint32_t D);

#endif
